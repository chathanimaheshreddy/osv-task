package com.example.osvtask;

import retrofit2.Call;
import retrofit2.http.GET;

interface APIInterface {

    @GET("/products")
    Call<ListModel> GetProductsList();
}