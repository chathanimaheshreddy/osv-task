package com.example.osvtask.adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.example.osvtask.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageAdapter extends PagerAdapter {


private Context mContext;
private  List<String> mimageurl;
public ImageAdapter(Context mContext, List<String> imageurl){
          this.mContext = mContext;
          this.mimageurl=imageurl;
     }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
  container.removeView((View)object);
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
View view  = LayoutInflater.from(mContext).inflate(R.layout.imagelayoutviewpager,container, false);
        ImageView mImageView = view.findViewById(R.id.image_for_viewpager);

        Picasso.get().load(getImageAtPosition(position)).into(mImageView);

container.addView(view);
        return view;
    }
    @Override
    public int getCount() {
        return mimageurl.size();
    }

private String getImageAtPosition(int position){
    switch (position){
        default: return mimageurl.get(position);
    }
}
}
