package com.example.osvtask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.osvtask.adapter.ImageAdapter;
import com.google.android.material.tabs.TabLayout;
import java.util.Timer;
import java.util.TimerTask;

public class InformationView extends AppCompatActivity {
    TextView titleTextview,price,stock,brand,category,description,discount;
    RatingBar ratingBar;
    Product product;
    ViewPager mViewPager;
    ImageAdapter mImageAdapter;
    ImageView mLeftTraverse;
    ImageView mRightTraverse;
    Timer timer;
    int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Details View");
        product = (Product) getIntent().getSerializableExtra("data");
        initializeUI();
        clickActions();
        setDataToUI();


    }

    private void setDataToUI() {
        mImageAdapter = new ImageAdapter(InformationView.this, product.getImages());
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(mImageAdapter);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {

                if (currentPage == mImageAdapter.getCount()) {
                    currentPage = 0;
                }
                mViewPager.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 5000, 7000);


        titleTextview.setText(product.getTitle());
        ratingBar.setRating(product.getRating());
        price.setText("\u20B9"+product.getPrice());
        stock.setText(product.getStock().toString());
        brand.setText(product.getBrand());
        category.setText(product.getCategory());
        description.setText(product.getDescription());
        discount.setText(product.getDiscountPercentage().toString()+"% Discount");
    }

    private void clickActions() {
        mLeftTraverse.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mViewPager.getCurrentItem() >= 1) {
                            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);
                        }
                    }
                }

        );

        mRightTraverse.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mViewPager.getCurrentItem() >= 0) {
                            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
                        }
                    }
                });
    }

    private void initializeUI() {
        mViewPager = findViewById(R.id.viewpager);
        TabLayout mTabLayout = findViewById(R.id.mtab_layout);
        mTabLayout.setupWithViewPager(mViewPager, true);
        mLeftTraverse = findViewById(R.id.left_indicator);
        mRightTraverse = findViewById(R.id.right_indicator);
        ratingBar = findViewById(R.id.ratingbar);
        titleTextview = findViewById(R.id.title);
        price = findViewById(R.id.price);
        stock = findViewById(R.id.stock);
        brand = findViewById(R.id.brand);
        category = findViewById(R.id.category);
        description = findViewById(R.id.description);
        discount = findViewById(R.id.discount);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
